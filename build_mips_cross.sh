#/bin/sh

cd /tmp\
&& git clone https://gitlab.com/pantacor/platform-tools/docker-musl-cross-mips.git\
&& cd docker-musl-cross-mips\
&& docker build -f Dockerfile -t musl-cross

