FROM registry.hub.docker.com/seresearch/debian-mipsel:latest
FROM registry.gitlab.com/pantacor/platform-tools/docker-musl-cross-mips

RUN apt-get update && apt-get install -y locales curl build-essential bc libpixman-1-dev libglib2.0-dev mtd-utils device-tree-compiler u-boot-tools lsb-release git-core python-pip parted dosfstools libncurses5-dev mtools texinfo squashfs-tools python3 vim-tiny libtool automake kmod bison flex cmake libpng-dev nodejs libcap-ng-dev libfdt-dev libgtk2.0-dev && apt-get clean

RUN pip install pyyaml
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -

RUN apt-get install -y nodejs

WORKDIR /lowjs_src

ADD ./ ./

ENV PATH="$PATH:/opt/cross/mips-linux-musl/bin/"
ENV TARGET_OUT_STAGING="/work/staging"

RUN [ ! -d ${TARGET_OUT_STAGING} ] && mkdir -p ${TARGET_OUT_STAGING}/lib
RUN cp -pv /opt/cross/mips-linux-musl/mips-linux-musl/lib/libstdc++.so* ${TARGET_OUT_STAGING}/lib

WORKDIR /lowjs_src

RUN ./build.sh $(pwd)

ENTRYPOINT /bin/bash
