#!/bin/bash

[ $# -ne 1 ] &&  "echo Usage $0 <path_to_lowjs_src>" && exit 1

TARGET_ARCH=mips
TARGET_CROSS=mips-linux-musl-
HOST_CC=cc
HOST_CXX=c++
TARGET_CC=${TARGET_CROSS}gcc

LOWJS_SRC_DIR="$1"
#TARGET_OUT_STAGING comes for dockerfile

[ ! -d $TARGET_OUT_STAGING ] || mkdir -p $TARGET_OUT_STAGING


C_ARES_CONFIGURE_OPTIONS='--host='${TARGET_ARCH}

LOWJS_BUILD_FLAGS=\
'ARCH='${TARGET_ARCH}' '\
'CC='"${TARGET_CC}"' '\
'HOSTCC='"${HOST_CC}"' '\
'HOSTCXX='"${HOST_CXX}"' '\
'CXX='"${TARGET_CROSS}g++"' '\
'AR='"${TARGET_CROSS}ar"' '\
'CROSS_COMPILE='"${TARGET_CROSS}"' '\
'CROSS='"${TARGET_CROSS}"' '\
'CONFIG_PREFIX='"${TARGET_OUT_STAGING}"' '\
'PREFIX='"${TARGET_OUT_STAGING}"' '\
'SRCDIR='"${LOWJS_SRC_DIR}"' '\
'C_ARES_CONFIGURE_OPTIONS='"${C_ARES_CONFIGURE_OPTIONS}"

echo "LOWJS_BUILD_FLAGS=$LOWJS_BUILD_FLAGS"

for make_var in $(echo $LOWJS_BUILD_FLAGS)
do
	export $(echo $make_var)
done

make -C ${LOWJS_SRC_DIR}
STRIP=${TARGET_CROSS}strip
${STRIP} ${LOWJS_SRC_DIR}/bin/*
${STRIP} ${LOWJS_SRC_DIR}/lib/*
echo "Copying Stripped binaries and libraries..."\
&& cp -prv ${LOWJS_SRC_DIR}/bin ${TARGET_OUT_STAGING}\
&& cp -prv ${LOWJS_SRC_DIR}/lib ${TARGET_OUT_STAGING}\
&& rm -fv /lowjs.squashfs\
&& mksquashfs ${TARGET_OUT_STAGING}/* /lowjs.squashfs -comp xz
